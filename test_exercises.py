from pathlib import Path
from shutil import copy
import pytest
from subprocess import run, PIPE


def run_solution(tmpdir):
    out = run(
        ["python3", "pre_check.py"], cwd=tmpdir, stdout=PIPE, stderr=PIPE, text=True
    )
    assert out.returncode == 0
    return run(["python3", "check.py"], cwd=tmpdir, stdout=PIPE, stderr=PIPE, text=True)


@pytest.mark.parametrize(
    "solution", (str(x) for x in Path("exercises").glob("*/solution.py"))
)
def test_solutions(solution, tmpdir):
    solution = Path(solution)
    exercise = solution.parent
    copy(solution, tmpdir)
    copy(exercise / "check.py", tmpdir)
    copy(exercise / "pre_check.py", tmpdir)
    result = run_solution(tmpdir)
    assert result.returncode == 0


@pytest.mark.parametrize(
    "solution", (str(x) for x in Path("exercises").glob("*/ok-*.py"))
)
def test_should_pass(solution, tmpdir):
    solution = Path(solution)
    exercise = solution.parent
    copy(solution, Path(tmpdir) / "solution.py")
    copy(exercise / "check.py", tmpdir)
    copy(exercise / "pre_check.py", tmpdir)
    result = run_solution(tmpdir)
    assert result.returncode == 0
    assert result.stderr == ""
    assert result.stdout == ""


@pytest.mark.parametrize(
    "solution", (str(x) for x in Path("exercises").glob("*/ko-*.py"))
)
def test_should_fail(solution, tmpdir):
    solution = Path(solution)
    exercise = solution.parent
    copy(solution, Path(tmpdir) / "solution.py")
    copy(exercise / "check.py", tmpdir)
    copy(exercise / "pre_check.py", tmpdir)
    result = run_solution(tmpdir)
    assert result.returncode == 1
    assert result.stderr + result.stdout
    assert not result.stderr.startswith("Traceback")
