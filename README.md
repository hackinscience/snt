# Exercices de SNT

Ce repo à pour but d'être synchronisé sur https://hackinscience.org/,
dans une page spécifique (qu'il reste à créer).

Pour plus d'informations sur hackinscience :
https://framagit.org/hackinscience/hkis-website


## Création d'un exercice

Un exercice se compose de quelques fichiers, en particulier :

- `wording_fr.md` le sujet, en français, en markdown, c'est ce qui est
  affiché dans la colonne de gauche sur HackInScience.
- `check.py` c'est le script lancé par HackInScience lorsqu'un élève envoie un rendu.
- `solution.py` c'est la solution à l'exercice, elle sert à vérifier le script de correction.

D'autres fichiers peuvent être présents :

- `wording_en.md` le sujet, traduit en anglais.
- `ko-1.py`, `ko-2.py`, ... sont des solutions qui sont connues pour
  ne pas fonctionner, elles ne servent qu'a des fins de test de la
  moulinette de correction.
- `ok-1.py`, `ok-2.py`, ... sont des solutions alternatives (qui
  doivent fonctionner), elles ne servent qu'a des fins de test de la
  moulinette de correction, en plus du `solution.py`.
- `meta` sont les métadonnées de l'exercice (auteur, position dans la
  liste, catégorie, ...)
- `initial_solution.py` c'est ce qu'HackInScience pré-remplit dans
  l'éditeur de code (typiquement un prototype de fonction).
- `pre_check.py` est exécuté *avant* `check.py` mais avec un accès à
  internet, contrairement à `check.py`, ça permet de pré-installer des
  composants nécessaires à la correction. Rarement utilisé.


## Fonctionnement de système de correction

Lorsqu'un élève clique sur « Envoyer » :
 - Le `pre_check.py`, s'il est présent, est exécuté.
 - Son code de l'élève est placé dans un fichier `solution.py`, afin de pouvoir l'importer, ou l'exécuter.
 - Le code `check.py` est exécuté dans une sandbox.
    - Si `check.py` `exit(0)` l'exercice est considéré réussi, optinnellement `check.py` peut afficher un message de félicitation (en Markdown).
    - Si `check.py` `exit(1)` l'exercice est considéré échoué, `check.py` doit afficher un message d'explication (en Markdown).


## Tester les moulinettes de correction

Pour tester les scripts de correction utilisez [tox](https://pypi.org/p/tox/).


## `correction_helper`

J'utilise énormément le projet
[correction_helper](https://github.com/JulienPalard/correction-helper)
dans mes moulinettes de correction, il permet d'éviter des
redondances, par exemple:

- `fail(message*)`: Affiche les message, séparés par `\n\n` (des
  paragraphes Markdown)et `exit(1)`,
- `code(str)`: Renvoie `str` sous forme d'un block de code Markdown
- `student_code` est un gestionnaire de contexte pour capturer les
  exceptions lancées par les élèves et les expliquer via
  [friendly-traceback](https://github.com/aroberge/friendly-traceback/).

Par exemple :

```python
with student_code():
    result = circle_perimeter(radius=1)
if result is None:
    fail("Votre fonction renvoie `None`, elle devrait renvoyer un nombre.")
if result != math.tau:
    fail("Le périmètre d'un cercle vaut `2 × π × r`.",
         "Un appel à votre fonction `circle_perimeter(radius=1)` a renvoyé :",
         code(result))
```
